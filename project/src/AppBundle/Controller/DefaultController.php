<?php

namespace AppBundle\Controller;

use AppBundle\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends FrontendController
{
    public function defaultAction(Request $request)
    {
    }
}
