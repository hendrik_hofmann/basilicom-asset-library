basilicom-asset-library
====================

**TODO** PROJECT DESCRIPTION - few sentences

## Table of contents

- [Development system setup](#markdown-header-development-system-setup)
- [Quickstart](#markdown-header-quickstart)
- [Deployment](#markdown-header-deployment)
- [Git Workflow](#markdown-git-workflow)
- [Testing](#markdown-git-testing)
- [Troubleshooting](#markdown-git-troubleshotting)
- Coding guides:
    - [Frontend development guide](docs/frontend-development.md)
    - [Backend development guide](docs/backend-development.md)
- [Authors](#markdown-git-authors)


## Development system setup 

**TODO** Describe what to do to get actual working system locally - possibly just `make` 


## Quickstart

The following command must be run only once:
```
make setup
``` 

Run this command to start the environment:
```
make start
```

In case the project uses any watchers:
```
make watch
```


## Deployment

**TODO** Describe what are special things about deployment - cloudfront caching, etc.


## Git Workflow

**TODO** 
Describe Branch Naming.
Describe any versioning practices. 
What triggers automatic deployments?


## Testing

* which technologies
    * PHPUnit
    * Behat
* how to setup PhpStorm?
    * PHP CS Fixer


## Troubleshooting

* where to find logs
* any logging service used?
* what to restart when something does not work
* turn on debug mode


## Authors

**TODO** List all authors involved in the project